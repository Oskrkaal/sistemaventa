# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'sistema.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(882, 633)
        MainWindow.setStyleSheet(_fromUtf8("background-color: rgb(255, 255, 255);"))
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.widget = QtGui.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(0, -10, 361, 651))
        self.widget.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.widget.setStyleSheet(_fromUtf8("background-color: rgb(71, 160, 255);"))
        self.widget.setObjectName(_fromUtf8("widget"))
        self.bt_entrar = QtGui.QPushButton(self.widget)
        self.bt_entrar.setGeometry(QtCore.QRect(150, 400, 91, 31))
        self.bt_entrar.setStyleSheet(_fromUtf8("background-color: rgb(111, 111, 111);\n"
"font: 16pt \"Decker\";\n"
"color: rgb(255, 255, 255);"))
        self.bt_entrar.setObjectName(_fromUtf8("bt_entrar"))
        self.layoutWidget = QtGui.QWidget(self.widget)
        self.layoutWidget.setGeometry(QtCore.QRect(40, 310, 281, 78))
        self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
        self.gridLayout = QtGui.QGridLayout(self.layoutWidget)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.label_2 = QtGui.QLabel(self.layoutWidget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Decker"))
        font.setPointSize(14)
        self.label_2.setFont(font)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout.addWidget(self.label_2, 0, 0, 1, 1)
        self.txt_pass = QtGui.QLineEdit(self.layoutWidget)
        self.txt_pass.setStyleSheet(_fromUtf8("font: 14pt \"Decker\";\n"
"color: rgb(255, 255, 255);\n"
"border-color: rgb(120, 120, 120);"))
        self.txt_pass.setObjectName(_fromUtf8("txt_pass"))
        self.gridLayout.addWidget(self.txt_pass, 1, 1, 1, 1)
        self.txt_usuario = QtGui.QLineEdit(self.layoutWidget)
        self.txt_usuario.setStyleSheet(_fromUtf8("font: 14pt \"Decker\";\n"
"color: rgb(255, 255, 255);\n"
"border-color: rgb(120, 120, 120);"))
        self.txt_usuario.setObjectName(_fromUtf8("txt_usuario"))
        self.gridLayout.addWidget(self.txt_usuario, 0, 1, 1, 1)
        self.label_3 = QtGui.QLabel(self.layoutWidget)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Decker"))
        font.setPointSize(14)
        self.label_3.setFont(font)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.gridLayout.addWidget(self.label_3, 1, 0, 1, 1)
        self.label = QtGui.QLabel(self.widget)
        self.label.setGeometry(QtCore.QRect(120, 150, 150, 150))
        self.label.setText(_fromUtf8(""))
        self.label.setPixmap(QtGui.QPixmap(_fromUtf8("Imagenes/003-login.png")))
        self.label.setScaledContents(True)
        self.label.setObjectName(_fromUtf8("label"))
        self.layoutWidget.raise_()
        self.bt_entrar.raise_()
        self.label.raise_()
        self.calendarWidget = QtGui.QCalendarWidget(self.centralwidget)
        self.calendarWidget.setGeometry(QtCore.QRect(600, 10, 264, 174))
        self.calendarWidget.setObjectName(_fromUtf8("calendarWidget"))
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "Sistema", None))
        self.bt_entrar.setText(_translate("MainWindow", "Entrar", None))
        self.label_2.setText(_translate("MainWindow", "<html><head/><body><p align=\"right\"><span style=\" font-size:16pt; color:#ffffff;\">Usuario:</span></p></body></html>", None))
        self.label_3.setText(_translate("MainWindow", "<html><head/><body><p align=\"right\"><span style=\" font-size:16pt; color:#ffffff;\">Password:</span></p></body></html>", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

